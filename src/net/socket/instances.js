import { EqSocket } from './eqsocket';

export const LoginSocket = new EqSocket();
export const WorldSocket = new EqSocket();
export const ZoneSocket = new EqSocket();